// lapsha-lapsha
class Chart {
  constructor(container, data, {height}) {
    const minimapHeight = 50;
    const width = container.clientWidth;
    const dataLength = data.columns[0].length;
    const sliderLeft = 60;
    const sliderRight = 90;

    // main canvas
    const canvasWrapper = document.createElement('div');
    canvasWrapper.className = 'canvas-wrapper';
    const canvas = document.createElement('canvas');
    canvas.height = height;
    canvas.width = width;

    // minimap canvas
    const canvasMinimap = document.createElement('canvas');
    canvasMinimap.height = minimapHeight;
    canvasMinimap.width = width;

    const minimap = document.createElement('div');
    const minimapController = document.createElement('div');
    const sliderLeftResize = document.createElement('span');
    const sliderRightResize = document.createElement('span');

    minimap.className = 'minimap';
    minimapController.className = 'minimapController';
    sliderLeftResize.className = 'resize left';
    sliderRightResize.className = 'resize right';

    minimap.addEventListener('click', this.click);
    minimap.addEventListener('click', this.click);
    window.addEventListener('mousemove', this.pointerMove);
    window.addEventListener('touchmove', this.pointerMove);
    minimapController.addEventListener('mousedown', this.pointerSet);
    minimapController.addEventListener('touchdown', this.pointerSet);
    window.addEventListener('mouseup', this.pointerReset);
    window.addEventListener('touchend', this.pointerReset);

    minimap.appendChild(minimapController);
    minimap.appendChild(canvasMinimap);
    minimapController.appendChild(sliderLeftResize);
    minimapController.appendChild(sliderRightResize);

    canvasWrapper.appendChild(canvas);
    container.appendChild(canvasWrapper);
    container.appendChild(minimap);

    this.container = container;
    this.canvasWrapper = canvasWrapper;
    this.canvas = canvas;
    this.canvasMinimap = canvasMinimap;
    this.minimap = minimap;
    this.minimapController = minimapController;

    this.data = data;
    this.height = height;
    this.minimapHeight = minimapHeight;
    this.width = width;
    this.fI = 1; // skip label - el with i 0
    this.lI = dataLength;
    this.dataLength = dataLength;
    this.sliderMin = 0;
    this.sliderMax = dataLength; // replace with data length

    const enabledLines = {};
    for (const i in data.names) {
      enabledLines[i] = true;
    }
    this.enabledLines = enabledLines;

    this.renderMinimap();
    this.slideTo(sliderLeft, sliderRight);
    this.renderControls();
  }

  renderControls() {
    const controls = document.createElement('div');
    controls.className = 'controls';

    for (const i in this.data.names) {
      const checkboxWrapper = document.createElement('div');
      const checkbox = document.createElement('input');
      const checkboxLabel = document.createElement('label');
      checkboxLabel.style.color = this.data.colors[i];
      checkboxLabel.for = i
      checkboxLabel.innerText = this.data.names[i];
      checkbox.type = 'checkbox';
      checkbox.name = i;
      checkbox.id = i;
      checkbox.onchange = this.handleCheckboxChange;
      
      checkboxWrapper.appendChild(checkbox);
      checkboxWrapper.appendChild(checkboxLabel);
      controls.appendChild(checkboxWrapper);

      checkbox.checked = this.enabledLines[i];
    }

    this.container.appendChild(controls);
    this.controls = controls;
  }

  handleCheckboxChange = (e) => {
    this.enabledLines[e.target.name] = e.target.checked;
    this.renderChart();
    this.renderMinimap();
  }

  slideTo(sliderLeft, sliderRight) {
    if (sliderRight - sliderLeft < 5) {
      return;
    }

    this.sliderLeft = sliderLeft;
    this.sliderRight = sliderRight;
    const percent = this.sliderMax / 100;
    const sliderLeftPercent = sliderLeft / percent;
    const sliderRightPercent = sliderRight / percent;
    this.minimapController.style.width = `${sliderRightPercent - sliderLeftPercent}%`;
    this.minimapController.style.left = `${sliderLeftPercent}%`;

    this.fI = sliderLeft === 0 ? 1 : sliderLeft; // skip label - el with i 0
    this.lI = sliderRight;

    this.renderChart();
  }

  pixelToPercent = (x) => {
    const minimapWidth = this.minimap.clientWidth;
    const scrollPercent = Number((x * 1 / minimapWidth * this.sliderMax).toFixed());
    return scrollPercent;
  }

  handleSliderMove = (x) => {
    const valueX = this.pixelToPercent(x);
    const sliderWidth = this.sliderRight - this.sliderLeft;
    const _newSliderLeft = valueX - sliderWidth / 2;
    const _newSliderRight = valueX + sliderWidth / 2;

    let newSliderLeft = _newSliderLeft;
    let newSliderRight = _newSliderRight;

    if (_newSliderRight > this.sliderMax) {
      newSliderRight = this.sliderMax;
      newSliderLeft = this.sliderMax - sliderWidth;
    } else if (_newSliderLeft < this.sliderMin) {
      newSliderLeft = this.sliderMin;
      newSliderRight = this.sliderMin + sliderWidth
    }

    this.slideTo(newSliderLeft, newSliderRight);
  }

  handleSliderResize = (x) => {
    const valueX = this.pixelToPercent(x);

    if (this.mouseDownResizeLeft) {
      const newSliderLeft = valueX < this.sliderMin ? this.sliderMin : valueX;
      this.slideTo(newSliderLeft, this.sliderRight);
    } else if (this.mouseDownResizeRight) {
      const newSliderRight = valueX > this.sliderMax ? this.sliderMax : valueX;
      this.slideTo(this.sliderLeft, newSliderRight);
    }
  }

  pointerMove = (e) => {
    if (this.mouseDown) {
      e.preventDefault();
      this.handleSliderMove(e.x);
    }
    if (this.mouseDownResizeLeft || this.mouseDownResizeRight) {
      e.preventDefault();
      this.handleSliderResize(e.x);
    }
  }

  click = (e) => {
    if (e.target.className.indexOf('minimapController') > -1
      || this.mouseDownResizeLeft
      || this.mouseDownResizeRight
      || this.mouseDown
    ) {
      return;
    }
    const mouseX = e.touches ? e.touches[0].layerX : e.layerX;
    this.handleSliderMove(mouseX);
  }

  pointerReset = (e) => {
    setTimeout(() => {
      this.mouseDown = false;
      this.mouseDownResizeRight = false;
      this.mouseDownResizeLeft = false;
    }, 100);
  }

  pointerSet = (e) => {
    if (e.target.className.indexOf('resize') > -1) {
      if (e.target.className.indexOf('left') > -1) {
        this.mouseDownResizeLeft = true;
      } else if (e.target.className.indexOf('right') > -1) {
        this.mouseDownResizeRight = true;
      }
      return;
    } else {
      this.mouseDown = true;
    }
  }

  renderChart = () => {
    const linesToRender = [];
    const maxValues = [];

    for (const i in this.data.columns) {
      const column = this.data.columns[i];
      const columnLabel = column[0];
      if (this.enabledLines[columnLabel]) {
        // const columnClean = column.slice(1);
        const columnClean = column.slice(this.fI, this.lI)

        // max value
        const _columnClean = [...columnClean];
        _columnClean.sort((a, b) => a > b ? -1 : 1)[0];
        maxValues.push(_columnClean[0]);

        linesToRender.push({
          data: columnClean,
          label: columnLabel,
        });
      }
    }
    const maxY = maxValues.sort((a, b) => a > b ? -1 : 1)[0];
    
    const elNToRender = this.lI - this.fI;

    var ctx = this.canvas.getContext('2d');
    ctx.lineWidth = 2;
    ctx.lineJoin = 'round'

    const withScale = this.width / (elNToRender - 1);
    const heightScale = this.height / maxY;
    
    ctx.clearRect(0, 0, this.width, this.height);

    linesToRender.forEach(line => this.renderLine(ctx, line.data, heightScale, withScale, line.label, this.height))
  }

  async renderLine(ctx, elToRenderArr, heightScale, withScale, label, height) {
    let x = 0;
    let i = 0;

    ctx.strokeStyle = this.data.colors[label];

    ctx.beginPath();
    ctx.moveTo(x, height - elToRenderArr[i] * heightScale);

    for (; i < elToRenderArr.length; i++) {
      ctx.lineTo(x, height - elToRenderArr[i] * heightScale);
      x += withScale;
    }

    ctx.stroke();
  }

  renderMinimap = () => {
    const linesToRender = [];
    const maxValues = [];

    for (const i in this.data.columns) {
      const column = this.data.columns[i];
      const columnLabel = column[0];
      if (this.enabledLines[columnLabel]) {
        const columnClean = column.slice(1);

        // max value
        const _columnClean = [...columnClean];
        _columnClean.sort((a, b) => a > b ? -1 : 1)[0];
        maxValues.push(_columnClean[0]);

        linesToRender.push({
          data: columnClean,
          label: columnLabel,
        });
      }
    }
    const maxY = maxValues.sort((a, b) => a > b ? -1 : 1)[0];
    
    const elNToRender = this.data.columns[0].length - 1;

    var ctx = this.canvasMinimap.getContext('2d');
    ctx.lineWidth = 1;
    ctx.lineJoin = 'round'

    const withScale = this.width / (elNToRender - 1);
    const heightScale = this.minimapHeight / maxY;
    
    ctx.clearRect(0, 0, this.width, this.minimapHeight);

    linesToRender.forEach(line => this.renderLine(ctx, line.data, heightScale, withScale, line.label, this.minimapHeight))
  }
}
